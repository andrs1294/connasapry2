/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.servidor;

import modelo.servidor.Servidor;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OptionalDataException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author SALAS
 */
public class clienteSocket extends Thread {

    private int user;
    private ObjectInputStream ois;
    private ObjectOutputStream oos;
    private Servidor servidor;
    private Socket sock;

    public Socket getSock() {
        return sock;
    }

    public void setOis(ObjectInputStream ois) {
        this.ois = ois;
    }

    public void setOos(ObjectOutputStream oos) {
        this.oos = oos;
    }

    
    public clienteSocket(int user, Servidor obj, Socket soc,ObjectInputStream ois, ObjectOutputStream oos) {
        this.user = user;
        this.sock=soc;
        this.ois = ois;
        this.oos = oos;
        this.servidor = obj;

    }

    @Override
    public String toString() {
        return "Pantalla " + user; //To change body of generated methods, choose Tools | Templates.
    }

    public void enviarMensaje(Object... msj) throws IOException {

        oos.writeObject(msj);

    }

    public ObjectOutputStream getOos() {
        return oos;
    }

    @Override
    public void run() {
        while (true) {
            Object accion = null;
            try {
                accion =  ois.readObject();
                
                servidor.accion(accion);
            } catch (java.io.OptionalDataException ex) {
                try {
                    ois.skipBytes(ois.available());
                } catch (IOException ex1) {
                    Logger.getLogger(clienteSocket.class.getName()).log(Level.SEVERE, null, ex1);
                }
            } catch (java.io.StreamCorruptedException ex) {
                try {
                    System.out.println("Intento reseteo");
                    ois.close();
                    
                    //ois.skipBytes(ois.available());
                } catch (IOException ex1) {
                    Logger.getLogger(clienteSocket.class.getName()).log(Level.SEVERE, null, ex1);
                }
            } catch (IOException ex) {
                Logger.getLogger(clienteSocket.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(clienteSocket.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }

    /**
     * @return the user
     */
    public int getUser() {
        return user;
    }

}
