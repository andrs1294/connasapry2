/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.servidor;

import modelo.servidor.clienteSocket;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.cliente.CuerpoAstronomico;
import vista.servidor.GUIServidor;

/**
 *
 * @author chevi
 */
public class Servidor extends Thread {

    private ServerSocket ssocket;
    private int puerto;
    private Vector<clienteSocket> listaClientes;
    private Vector<CuerpoAstronomico> cuerposAstronomicos;
    private Vector<Integer> cuerposAstronomicosCantidad;
    private int ctdClientes;
    private boolean clientesCompletos;
    private GUIServidor gui;

    public Servidor(int puerto, int ctdClientes, GUIServidor gui) throws IOException {
        this.puerto = puerto;
        Configurar();
        this.ctdClientes = ctdClientes;
        cuerposAstronomicos = new Vector<>();
        cuerposAstronomicosCantidad = new Vector<>();
        this.gui = gui;
        listaClientes = new Vector<>();

        this.start();

    }

    public void AtenderSaludo() throws IOException {

        Socket socketCliente = ssocket.accept();
        ObjectOutputStream oos = new ObjectOutputStream(socketCliente.getOutputStream());
        if (!clientesCompletos) {
            ObjectInputStream ois = new ObjectInputStream(socketCliente.getInputStream());
            clienteSocket obj = new clienteSocket(listaClientes.size() + 1, this, socketCliente, ois, oos);
            agregarCliente(obj);

            if (clientesCompletos) {
                enviarATodos("jugar");
                //this.stop();
            }
            notificarGUIClientesConectados();

        } else {
            oos.writeBoolean(false);

            socketCliente.close();
        }

    }

    public synchronized void accion(Object msj2) {
        Object msj[] = null;
        if (msj2 == null) {
            return;
        }
        if (msj2 instanceof Object[]) {
            msj = (Object[]) msj2;
        } else if (msj2 instanceof String) {
            return;
        }

        if (msj[0] instanceof CuerpoAstronomico) {
            CuerpoAstronomico cp = (CuerpoAstronomico) (msj[0]);
            crearCuerpoAstronomico(cp);
        } else if (msj[0] instanceof Integer) {
            int userDestino = (int) msj[0];
            if (msj[1] instanceof CuerpoAstronomico) {
                CuerpoAstronomico cp = (CuerpoAstronomico) (msj[1]);
                int x = (int) msj[2];
                int y = (int) msj[3];

                boolean enviado = enviarCrearCuerpo(userDestino, cp, x, y);
                if (enviado) {
                    setCantidadCuerpos(cp, 1);
                }
            }

        } else if (msj[0] instanceof String) {
            String mensaje = (String) msj[0];
            if (mensaje.equalsIgnoreCase("eliminado")) {
                if (msj[1] instanceof CuerpoAstronomico) {
                    CuerpoAstronomico cp = (CuerpoAstronomico) (msj[1]);
                    setCantidadCuerpos(cp, -1);
                } else if (msj[1] instanceof Object[]) {
                    Object[] obj = (Object[]) msj[1];
                    if (obj[0] instanceof CuerpoAstronomico) {
                        CuerpoAstronomico cp = (CuerpoAstronomico) (msj[1]);
                        setCantidadCuerpos(cp, -1);
                    }
                } else {
                    System.out.println("No se ha eliminado algo");
                }

            }
        }

    }

    private synchronized void enviarATodos(Object... datos) {

        for (clienteSocket cliente : listaClientes) {
            try {
                cliente.enviarMensaje(datos);
            } catch (IOException ex) {
            }

        }
    }

    private void Configurar() throws IOException {
        ssocket = new ServerSocket(puerto);
    }

    public void setPuerto(int puerto) {
        this.puerto = puerto;
    }

    @Override
    public void run() {
        while (true) {
            try {
                AtenderSaludo();

            } catch (IOException ex) {
                Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void agregarCliente(clienteSocket obj) throws IOException {
        listaClientes.add(obj);
        obj.enviarMensaje(new String[]{listaClientes.size() + "", "ok"});
        obj.start();
        clientesCompletos = listaClientes.size() == ctdClientes;
    }

    public void eliminarCuerpoAstronomico(int id) {
        for (int i = 0; i < cuerposAstronomicos.size(); i++) {
            if (cuerposAstronomicos.get(i).getid() == id) {
                enviarATodos("eliminar", cuerposAstronomicos.get(i));
                cuerposAstronomicos.remove(i);
                cuerposAstronomicosCantidad.remove(i);
                break;
            }
        }
    }

    private void notificarGUIClientesConectados() {
        gui.mostrarClientesConectados(listaClientes.size());
    }

    private boolean enviarCrearCuerpo(int userDestino, CuerpoAstronomico enviado, int x, int y) {
        for (clienteSocket cli : listaClientes) {
            if (cli.getUser() == userDestino) {
                try {
                    cli.enviarMensaje(enviado, x, y);
                    return true;
                } catch (IOException ex) {
                    return false;
                }
            }
        }
        return false;
    }

    private CuerpoAstronomico crearCuerpoAstronomico(CuerpoAstronomico cuerpoAstronomico) {

        CuerpoAstronomico cuerpo = cuerpoAstronomico;
        cuerposAstronomicos.add(cuerpo);
        cuerposAstronomicosCantidad.add(1);
        gui.notificacionCuerpoNuevo(cuerpo);
        return cuerpo;
    }

    private void setCantidadCuerpos(CuerpoAstronomico cp, int i) {
        for (CuerpoAstronomico cuerpo : cuerposAstronomicos) {
            if (cuerpo.equals(cp)) {

                int pos = cuerposAstronomicos.indexOf(cp);
                cuerposAstronomicosCantidad.set(pos, cuerposAstronomicosCantidad.get(pos) + i);
                if (cuerposAstronomicosCantidad.get(pos) == 0) {
                    gui.eliminarCuerpo(cp);
                    cuerposAstronomicos.remove(pos);
                    cuerposAstronomicosCantidad.remove(pos);
                }
                break;
            }
        }
    }

}
