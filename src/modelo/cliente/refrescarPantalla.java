/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.cliente;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JPanel;

/**
 *
 * @author andrs1294
 */
public class refrescarPantalla extends Thread {

    JPanel gui;

    public refrescarPantalla(JPanel pn) {
        gui = pn;
        this.start();
    }

    @Override
    public void run() {
        while (true) {
            gui.repaint();
            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {
                Logger.getLogger(refrescarPantalla.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

}
