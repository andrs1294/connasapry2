/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.cliente;

import java.awt.Rectangle;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import vista.cliente.inicio;

/**
 *
 * @author chevi
 */
public class Cliente extends Thread {

    private Socket socket;
    private String host;
    private int puerto;
    private ObjectOutputStream oos;
    private ObjectInputStream ois;
    private int numeroEquipo;
    Vector<CuerpoAstronomico> cuerposAstronomicos;
    private refrescarPantalla refresco;
    private JPanel panel;
    private inicio gui;

    public Cliente(JPanel pn, inicio gui) throws IOException {
        this.host = "localhost";
        this.puerto = 20000;
        cuerposAstronomicos = new Vector<>();
        conectar();
        refresco = new refrescarPantalla(pn);
        panel = pn;
        this.gui = gui;
    }

    private void conectar() throws IOException {

        this.socket = new Socket(this.host, this.puerto);//establece en enlace de coneccion entre el soket cliente y el servidor
        this.oos = new ObjectOutputStream(this.socket.getOutputStream());//canal de salida de datos
        this.ois = new ObjectInputStream(this.socket.getInputStream());//canal de entrada de daos
    }

    public int Saludar() throws IOException, ClassNotFoundException {

        oos.writeObject("hola");
        Object respu = (ois.readObject());

        String respuesta[] = (String[]) respu;
        numeroEquipo = Integer.parseInt(respuesta[0]);

        return numeroEquipo;

    }

    public void accion(Object... argu) {

        try {
            oos.writeObject(argu);
        } catch (IOException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void run() {
        int vecesIntentarConectar = 0;
        while (true) {
            try {
                Object[] msj = (Object[]) ois.readObject();
                if (msj[0] instanceof CuerpoAstronomico) {
                    int x = (int) msj[1];
                    int y = (int) msj[2];
                    agregarCuerpoAstronomico((CuerpoAstronomico) (msj[0]), x, y);
                } else if (msj[0] instanceof String) {
                    String mensaje = (String) (msj[0]);
                    if (mensaje.equalsIgnoreCase("eliminar")) {
                        CuerpoAstronomico cp = (CuerpoAstronomico) (msj[1]);
                        eliminarCuerpoEnVista(cp);
                    } else if (mensaje.equalsIgnoreCase("jugar")) {
                        gui.setBotones(true);
                    }
                } else if (msj[0] instanceof Boolean) {
                    boolean aux = (boolean) msj[0];
                    if (!aux) {
                        gui.denegarEntrada();
                        this.stop();
                    }
                }
                //gui.mensajeEntrante(msj);
            } catch (IOException ex) {
                if (ex != null) {
                    if (ex.getMessage().equals("Broken pipe")) {
                        if (vecesIntentarConectar > 80) {
                            JOptionPane.showMessageDialog(panel, "Se ha perdido la coneccion, la ventana se cerrará");
                            gui.destruir();
                        }

                    }
                }

            } catch (Exception e) {
                System.out.println("Error entrante cliente socket");
            }
        }
    }

    public void generarCuerpo(CuerpoAstronomico.tipo tipo) {
        CuerpoAstronomico obj = new CuerpoAstronomico(tipo, this, panel);

        cuerposAstronomicos.add(obj);
        accion(obj);
        obj.start();

    }

    boolean informarChoqueFrontera(int i, CuerpoAstronomico cuerpo) {
        if (i == 1) {
            Object[] msj = {numeroEquipo - 1, cuerpo, panel.getBounds().width - cuerpo.getArea().x, cuerpo.getArea().y};
            accion(msj);
        } else if (i == 2) {
            Object[] msj = {numeroEquipo + 1, cuerpo, cuerpo.getArea().x - panel.getBounds().width, cuerpo.getArea().y};
            accion(msj);
        }
        return true;
    }

    void eliminarCuerpo(CuerpoAstronomico aThis) {
        cuerposAstronomicos.remove(aThis);
        accion("eliminado", aThis);
    }

    private void agregarCuerpoAstronomico(CuerpoAstronomico cuerpoAstronomico, int x, int y) {
        CuerpoAstronomico cp = cuerpoAstronomico;
        cp.setCli(this);
        cp.setBoundFrontera(panel);
        establecerAreaCuerpo(cp, x, y);
        cuerposAstronomicos.add(cp);
        cp.start();

    }

    private void establecerAreaCuerpo(CuerpoAstronomico cp, int x, int y) {
        Rectangle rc = cp.getArea();
        rc.setLocation(x, y);
    }

    private void eliminarCuerpoEnVista(CuerpoAstronomico cp) {
        for (CuerpoAstronomico cuerpo : cuerposAstronomicos) {
            if (cuerpo.getid() == cp.getid()) {
                cuerpo.stop();
                cuerposAstronomicos.remove(cp);
                break;
            }
        }
    }

    void generarExplosion(CuerpoAstronomico aThis, CuerpoAstronomico cuerpo) {
        panel pn = (panel) (this.panel);
        pn.generarExplosion(aThis, cuerpo);

    }

    void quitarCuerpos(CuerpoAstronomico cup1, CuerpoAstronomico cup2) {
        cup1.stop();
        cup2.stop();
        eliminarCuerpo(cup2);
        eliminarCuerpo(cup1);
    }

}
