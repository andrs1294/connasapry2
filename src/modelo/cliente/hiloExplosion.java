/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.cliente;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JPanel;

/**
 *
 * @author andrs1294
 */
public class hiloExplosion extends Thread{

    private panel pn;
    private CuerpoAstronomico cup1;
    private CuerpoAstronomico cup2;
    public hiloExplosion(panel pn,CuerpoAstronomico cup1,CuerpoAstronomico cup2) {
        this.pn = pn;
        this.cup1=cup1;
        this.cup2=cup2;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(1400);
            pn.quitarExplosion(cup1,cup2);
        } catch (InterruptedException ex) {
            Logger.getLogger(hiloExplosion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
}
