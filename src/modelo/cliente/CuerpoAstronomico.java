/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.cliente;

import java.awt.Rectangle;
import java.io.Serializable;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.Random;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JPanel;

/**
 *
 * @author andrs1294
 */
public class CuerpoAstronomico extends Thread implements Serializable {

    private int id;
    private int velocidadX;
    private int velocidadY;
    private Rectangle area;
    private transient boolean enviadoAOtros;
    private transient Cliente cli;
    private tipo tipo;
    private transient Rectangle pn;
    private boolean yaExploto;

    private boolean noSeVeEnPantalla() {
        boolean salePanelAnchoDer = this.area.x > pn.x + pn.width;
        boolean salePanelAnchoIzq = this.area.x + this.area.width < pn.x;

        return salePanelAnchoDer || salePanelAnchoIzq;
    }

    /**
     * @return the yaExploto
     */
    public boolean isYaExploto() {
        return yaExploto;
    }

    /**
     * @param yaExploto the yaExploto to set
     */
    public void setYaExploto(boolean yaExploto) {
        this.yaExploto = yaExploto;
    }

    public static enum tipo {

        luna, sol, galaxia
    }

    public CuerpoAstronomico(tipo type, Cliente cli, JPanel panel) {
        Random rn = new Random();
        pn = panel.getBounds();
        id = rn.nextInt();
        int x = rn.nextInt(pn.width - 30);
        int y = rn.nextInt(pn.height - 30);
        if (type == tipo.sol) {
            area = new Rectangle(x, y, 35, 35);
        } else if (type == tipo.luna) {
            area = new Rectangle(x, y, 25, 25);
        } else if (type == tipo.galaxia) {
            area = new Rectangle(x, y, 80, 65);
        }

        enviadoAOtros = false;
        this.cli = cli;
        tipo = type;
        velocidadX = rn.nextInt(10) + 4;
        velocidadY = rn.nextInt(3) + 3;
        velocidadX=(rn.nextInt(2)==1)?-velocidadX:velocidadX;
        velocidadY=(rn.nextInt(2)==1)?-velocidadY:velocidadY;

    }

    void setBoundFrontera(JPanel pn) {
        this.pn = pn.getBounds();
    }

    public int getid() {
        return id % 65536;
    }

    @Override
    public boolean equals(Object obj) {

        if (obj instanceof CuerpoAstronomico) {
            CuerpoAstronomico cp = (CuerpoAstronomico) (obj);
            return cp.getid() == this.getid();
        }
        return super.equals(obj);
    }

    @Override
    public String toString() {
        return "" + getType();
    }

    @Override
    public void run() {
        int cantidad = 0;
        enviadoAOtros = true;
        while (true) {
            mover();
            verificarChoqueFrontera();
            verificarChoqueOtroCuerpo();
            if (cantidad < 600) {
                cantidad += 100;
                if (cantidad == 600) {
                    enviadoAOtros = false;
                }
            }

            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {
                Logger.getLogger(CuerpoAstronomico.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ConcurrentModificationException ex) {
                eliminarCuerpo();
            }
        }

    }

    public void setCli(Cliente cli) {
        this.cli = cli;
    }

    private void verificarChoqueOtroCuerpo() {
        Vector<CuerpoAstronomico> lista = cli.cuerposAstronomicos;
        Iterator obj = lista.iterator();
        try {
            while (obj.hasNext()) {
                CuerpoAstronomico cuerpo = (CuerpoAstronomico) obj.next();
                if (cuerpo == this) {
                    continue;
                } else {
                    if (cuerpo.tipo == this.tipo) {
                        if (cuerpo.getArea().intersects(this.getArea())) {
                            //explosion
                            if (!this.yaExploto || !cuerpo.isYaExploto()) {
                                this.enviadoAOtros = true;
                                cuerpo.enviadoAOtros = true;
                                cli.generarExplosion(this, cuerpo);

                            }

                        }
                    }
                }
            }
        } catch (ConcurrentModificationException ex) {
            System.out.println("Error concurrente choque cuerpos");
            eliminarCuerpo();
        }

    }

    private void verificarChoqueFrontera() {
        boolean salePanelAnchoDer = this.area.x + this.area.width > pn.x + pn.width;
        boolean salePanelAnchoIzq = this.area.x < pn.x;
        boolean salePanelAbajo = this.area.y > pn.y + pn.height;
        boolean salePanelArriba = this.area.y + this.area.height < pn.y;
        if (!enviadoAOtros) {
            if (salePanelAnchoDer) {
                enviadoAOtros = cli.informarChoqueFrontera(2, this);
            } else if (salePanelAnchoIzq) {
                enviadoAOtros = cli.informarChoqueFrontera(1, this);
            }
        }

        if (enviadoAOtros || salePanelAbajo || salePanelArriba) {
            if (enviadoAOtros) {
                if (noSeVeEnPantalla()) {
                    eliminarCuerpo();
                }
            } else if (salePanelAbajo || salePanelArriba) {
                eliminarCuerpo();
            }

        }

    }

    private void eliminarCuerpo() {
        cli.eliminarCuerpo(this);
        this.stop();

    }

    public Rectangle getArea() {
        return area;
    }

    public tipo getType() {
        return tipo;
    }

    private void mover() {
        int x = area.x + velocidadX;
        int y = area.y + velocidadY;
        area.setLocation(x, y);
    }

}
