/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista.cliente;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import modelo.cliente.Cliente;
import modelo.cliente.CuerpoAstronomico;

/**
 *
 * @author andrs1294
 */
public class inicio extends javax.swing.JFrame {

    private Cliente cli;
    /**
     * Creates new form inicio
     */
    public inicio() {
        initComponents();
        configurarVista();
    }

   
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnCambio = new javax.swing.JButton();
        lblNumEquipo = new javax.swing.JLabel();
        panel1 = new modelo.cliente.panel();
        btnGalaxia = new javax.swing.JButton();
        btnSol = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        btnCambio.setText("Conectar");
        btnCambio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCambioActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panel1Layout = new javax.swing.GroupLayout(panel1);
        panel1.setLayout(panel1Layout);
        panel1Layout.setHorizontalGroup(
            panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        panel1Layout.setVerticalGroup(
            panel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 245, Short.MAX_VALUE)
        );

        btnGalaxia.setText("Galaxia");
        btnGalaxia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGalaxiaActionPerformed(evt);
            }
        });

        btnSol.setText("Sol");
        btnSol.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSolActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblNumEquipo, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnSol)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnCambio)
                .addGap(11, 11, 11)
                .addComponent(btnGalaxia)
                .addContainerGap(61, Short.MAX_VALUE))
            .addComponent(panel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(panel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblNumEquipo, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnCambio)
                        .addComponent(btnGalaxia)
                        .addComponent(btnSol)))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCambioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCambioActionPerformed
        try {
            if(btnCambio.getText().equals("Conectar"))
            {
                cli = new Cliente(panel1,this);
                int num = cli.Saludar();
                panel1.setCli(cli);
                lblNumEquipo.setText(num+"");
                btnCambio.setText("Luna");
                setBotones(false);
                
                cli.start();
            }else
            {
                 cli.generarCuerpo(CuerpoAstronomico.tipo.luna);
                
            }
            
        } catch (IOException ex) { 
            if (ex.getMessage().equals("Broken pipe")) {
                denegarEntrada();
            }else
            {
                JOptionPane.showMessageDialog(rootPane, "No es posible conectarse al servidor.");
            }
           
           
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(inicio.class.getName()).log(Level.SEVERE, null, ex);
        } 
        
    }//GEN-LAST:event_btnCambioActionPerformed

    private void btnGalaxiaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGalaxiaActionPerformed
        // TODO add your handling code here:
        cli.generarCuerpo(CuerpoAstronomico.tipo.galaxia);
    }//GEN-LAST:event_btnGalaxiaActionPerformed

    private void btnSolActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSolActionPerformed
        // TODO add your handling code here:
        cli.generarCuerpo(CuerpoAstronomico.tipo.sol);
    }//GEN-LAST:event_btnSolActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(inicio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(inicio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(inicio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(inicio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new inicio().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCambio;
    private javax.swing.JButton btnGalaxia;
    private javax.swing.JButton btnSol;
    private javax.swing.JLabel lblNumEquipo;
    private modelo.cliente.panel panel1;
    // End of variables declaration//GEN-END:variables

    private void configurarVista() {
     btnGalaxia.setEnabled(false);
      btnSol.setEnabled(false);
    }

    public void setBotones(boolean activar) {
       btnCambio.setEnabled(activar);
       btnGalaxia.setEnabled(activar);
       btnSol.setEnabled(activar);
    }

    public void denegarEntrada() {
       JOptionPane.showMessageDialog(rootPane, "No es posible conectarse al servidor, clientes completos");
    }

    public void destruir() {
       this.dispose();
    }
    
 
}

